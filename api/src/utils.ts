export class Utils {
    static isEmptyObject(obj: any) {
        if (!obj || typeof obj !== 'object') {
            return true;
        }

        const keys = Object.keys(obj);

        if (keys.length === 0) {
            return true;
        }

        return false;
    }
}
import { BaseEndpoint } from "./base";
import { BoozeModel } from "../service/booze";
import { BoozeEntity, BoozeError } from "../interface/booze";
import { Utils } from "../utils";

export class BoozeEndpoint  extends BaseEndpoint {
    constructor() {
        super();
    }

    async get(req: any, res: any) {
        const { id: boozeId } = req.params;

        try {
            const result: BoozeEntity = await BoozeModel.getBooze(boozeId);
            res.status(200).json(result);
        } catch (err) {
            res.status(404).json({code: BoozeError.CANNOT_GET, message: [`can't get requested booze: ${boozeId}`, err]});
        }
    }

    async post(req: any, res: any) {
        const data = req.body;

        if (Utils.isEmptyObject(data)) {
            res.status(401).json({code: BoozeError.EMPTY_DATA, message: ['new booze data cannot be empty']});
            return;
        }

        try {
            const result = await BoozeModel.createBooze(data);
            res.status(200).json({id: result});
        } catch (err) {
            res.status(401).json({code: BoozeError.CANNOT_CREATE, message: [`Can't create booze`, err]});
        }
    }

    async list(req: any, res: any) {
        try {
            const result: Array<BoozeEntity> = await BoozeModel.getAllBooze();
            res.status(200).json(result);
        } catch (err) {
            res.status(404).json({code: BoozeError.CANNOT_CREATE, message: [`can't get all boozes`, err]});
        }
    }

    async put(req: any, res: any) {
        const {id: boozeId} = req.params;
        const data: BoozeEntity = req.body;

        if (Utils.isEmptyObject(data)) {
            res.status(401).json({code: BoozeError.EMPTY_DATA, message: ['update booze data cannot be empty']});
            return;
        }

        try {
            const result = await BoozeModel.updateBooze(boozeId, data);
            res.status(200).json({boozesUpdated: result});
        } catch (err) {
            res.status(404).json({code: BoozeError.CANNOT_UPDATE, message: [`can't update booze: ${boozeId}`, err]});
        }
    }

    async delete(req: any, res: any) {
        const {id: boozeId} = req.params;

        try {
            const result = await BoozeModel.deleteBooze(boozeId);
            res.status(200).json(result);
        } catch (err) {
            res.status(404).json({code: BoozeError.CANNOT_DELETE, message: [`can't delete booze: ${boozeId}`, err]});
        }
    }
}
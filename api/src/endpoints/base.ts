export class BaseEndpoint {
    static registerEndpoint(endpointClass: any, path: string, router: any, appendId=true) {
        const pathWithId = appendId ? (path.slice(-1) === '/' ? path + ':id' : path + '/:id') : path;
        const instance = new endpointClass();

        if (typeof instance.list === 'function' && instance.list) router.route(path).get(instance.list);
        if (typeof instance.post === 'function' && instance.post) router.route(path).post(instance.post);
        if (typeof instance.get === 'function' && instance.get) router.route(pathWithId).get(instance.get);
        if (typeof instance.put === 'function' && instance.put) router.route(pathWithId).put(instance.put);
        if (typeof instance.delete === 'function' && instance.delete) router.route(pathWithId).delete(instance.delete);
    }
}

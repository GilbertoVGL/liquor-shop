export interface BoozeEntity {
    id: number,
    name: string,
    description: string,
    price: number,
    createdAt: Date,
    updatedAt: Date
}

export enum BoozeError {
    CANNOT_CREATE = "unable-to-create-booze",
    CANNOT_GET = "unable-to-get-booze",
    CANNOT_UPDATE = "unable-to-update-booze",
    CANNOT_DELETE = "unable-to-delete-booze",
    EMPTY_DATA = "empty-booze-data"
}
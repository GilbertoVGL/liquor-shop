import express = require("express");
import cors = require("cors");
import {
    BaseEndpoint,
    BoozeEndpoint
} from "./endpoints/index";
const app: express.Application = express();
const router = express.Router();
app.use(cors({ origin: true }));
app.use(express.json());

BaseEndpoint.registerEndpoint(BoozeEndpoint, '/booze', router, true);

app.use('/api', router);

app.listen(3000, function() {
    console.log('Server listening');
});
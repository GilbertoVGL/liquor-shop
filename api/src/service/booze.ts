const { Booze } = require("../db/models");

export class BoozeModel {

    static async getBooze(boozeId: number) {
        const booze = await Booze.findByPk(boozeId);
        return booze?.dataValues ?? null;
    }

    static async createBooze(body: any) {
        try {
            const newBooze = await Booze.create(body);
            return newBooze.id;
        } catch (err) {
            throw (err);
        }
    }

    static async getAllBooze() {
        try {
            const allBooze = await Booze.findAll();
            return allBooze;
        } catch (err) {
            throw (err);
        }
    }

    static async updateBooze(boozeId: string, boozeData: any) {
        try {
            const updatedBooze = await Booze.update(boozeData, {
                where: {
                  id: boozeId
                }
            });

            return updatedBooze.pop();
        } catch (err) {
            throw (err);
        }
    }

    static async deleteBooze(boozeId: string) {
        try {
            const deletedBooze = await Booze.destroy({where: {id: boozeId}});
            return deletedBooze;
        } catch (err) {
            throw(err);
        }
    }

}
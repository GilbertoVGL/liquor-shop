'use strict';

module.exports = {
	up: async (queryInterface, DataTypes) => {
		return queryInterface.createTable('Boozes', {
			id: {
				allowNull: false,
				autoIncrement: true,
				primaryKey: true,
				type: DataTypes.INTEGER,
			},
			name: {
				allowNull: false,
				type: DataTypes.STRING,
			},
			description: {
				type: DataTypes.STRING,
			},
			price: {
				allowNull: false,
				type: DataTypes.FLOAT,
			},
			createdAt: {
				allowNull: false,
				type: DataTypes.DATE,
			},
			updatedAt: {
				allowNull: false,
				type: DataTypes.DATE,
			}
		});
	},

	down: async (queryInterface, Sequelize) => {
		return queryInterface.dropTable('Boozes');
	}
};

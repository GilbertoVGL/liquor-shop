import axios from 'axios';
import { BoozeModel } from '../entities/booze';

export class BoozeService {
    static URL = "http://localhost:3000/api/booze";

    static async createBooze(body: any) {
        const response = await axios.post(BoozeService.URL, body);
        return response;
    }

    static async updateBooze(body: any, id: number) {
        const response: any = await axios.put((BoozeService.URL + `/${id}`), body);
        return response;
    }

    static async getBooze(id: number): Promise<BoozeModel> {
        const response: any = await axios.get((BoozeService.URL + `/${id}`));
        const data: BoozeModel = response.data;
        return data;
    }

    static async getBoozes(): Promise<Array<BoozeModel>> {
        const response: any = await axios.get(BoozeService.URL);
        const data: Array<BoozeModel> = response.data;
        return data;
    }

    static async deleteBooze(id: number) {
        const response: any = await axios.delete((BoozeService.URL + `/${id}`));
        return response;
    }
}
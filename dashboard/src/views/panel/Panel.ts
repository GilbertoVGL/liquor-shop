import { Component, Vue } from 'vue-property-decorator';
import NavBar from '../../components/navbar/NavBar.vue';

@Component({
    components: {
        NavBar
    },
})
export default class Panel extends Vue { }
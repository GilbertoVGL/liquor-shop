import { Component, Vue } from 'vue-property-decorator';
import { BoozeModel } from '../../entities/booze';
import { BoozeService } from '../../services/booze-service';
import router from '../../router/index';

@Component({
    components: { }
})
export default class BoozeList extends Vue{
    private loading = true;
    private sortBy = 'name';
    private sortDesc = false;
    private boozes: Array<BoozeModel> = [];
    private inputFilter = '';
    private currentPage = 1;
    private totalRows = 1;
    private perPage = 15;
    private fields = [
        {key: 'name', label: 'Nome', sortable: true}, 
        {key: 'description', label: 'Descição'}, 
        {key: 'price', label: 'Preço', sortable: true}, 
        {key: 'createdAt', label: 'Data de criação', sortable: true},
        {key: 'actions', label: ''}
    ];

    async mounted() {
        this.boozes = await BoozeService.getBoozes();
        this.totalRows = this.boozes.length;
        this.loading = false;
        return;
    }

    editBooze(boozes: Array<BoozeModel>) {
        const booze: any = boozes.pop();
        router.push({name: 'New', params: booze});
    }

    async deleteBooze(booze: BoozeModel, index: number) {
        this.loading = true;
        try {
            const result = await BoozeService.deleteBooze(booze.id);
            if (result.data > 0) {
                this.boozes.splice(index, 1);
                Vue.$toast.success('Bebida deletada com sucesso.');
            }

        } catch (err) {
            Vue.$toast.error(`Não foi possível deletar a bebida ${booze.name}`);
        } finally {
            this.loading = false;
        }
    }

    onFiltered(filteredItems: any) {
        this.totalRows = filteredItems.length
        this.currentPage = 1
    }
}
import { Component, Vue } from 'vue-property-decorator';
import { BoozeService } from '../../services/booze-service';

@Component({
    components: { }
})
export default class NewBooze extends Vue {
    private booze = {
        id: 0,
        name: '',
        price: 0,
        description: ''
    };
    private editing = false;
    private loading = false;

    get isValidName(): boolean {
        return this.booze.name?.length >= 5 ? true : false;
    }

    get invalidName() {
        if (this.booze.name?.length > 5) {
            return ''
        } else if (this.booze.name?.length > 0) {
            return 'O nome precisa ter no mínimo 5 caracteres'
        } else {
            return 'Por favor, digite o nome da bebida'
        }
    }

    get validPrice() {
        return this.booze.price;
    }

    set validPrice(newPrice: number) {
        this.booze.price = newPrice ? Math.abs(Number(newPrice)) : 0;
    }

    get validForm() {
        return true;
    }

    created() {
        const booze = this.$route.params;
        if (Object.keys(booze).length > 0) {
            this.booze.id = Number(booze.id);
            this.booze.name = booze.name;
            this.booze.price = Number(booze.price);
            this.booze.description = booze.description;
            this.editing = true;
        }

        return;
    }

    async saveBooze() {
        this.loading = true;
        const body = {
            name: this.booze.name,
            price: this.booze.price,
            description: this.booze.description
        };

        try {
            await BoozeService.createBooze(body);
            this.resetForm();
            Vue.$toast.success(`Bebida ${body.name} criada com sucesso.`);
        } catch (err) {
            Vue.$toast.error('Não foi possível registrar a bebida.');
        } finally {
            this.loading = false;
        }
    }

    async updateBooze() {
        const body = {
            name: this.booze.name,
            price: this.booze.price,
            description: this.booze.description
        };

        try {
            await BoozeService.updateBooze(body, this.booze.id);
            this.resetForm();
            Vue.$toast.success(`Bebida ${body.name} atualizada com sucesso.`);
        } catch (err) {
            Vue.$toast.error('Não foi possível atualizar a bebida.');
        } finally {
            this.loading = false;
        }
    }

    resetForm() {
        this.booze = {
            id: 0,
            name: '',
            price: 0,
            description: ''
        };
        this.editing = false;
    }
}
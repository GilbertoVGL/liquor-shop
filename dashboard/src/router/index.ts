import Vue from 'vue';
import VueRouter, { RouteConfig } from 'vue-router';

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
	{
		path: '',
		redirect: '/panel/list'
	},
	{
		path: '/',
		redirect: '/panel/list'
	},
	{
		path: '/panel',
		name: 'Panel',
		component: () => import('../views/panel/Panel.vue'),
		children: [
			{
				path: 'new',
				name: 'New',
				component: () => import('../views/newBooze/NewBooze.vue')
			},
			{
				path: 'list',
				name: 'List',
				component: () => import('../views/boozeList/BoozeList.vue')
			}
		]
	}
]

const router = new VueRouter({
	routes
});

export default router;

export interface BoozeModel {
    id: number,
    name: string,
    description: string,
    price: number,
    createdAt: Date,
    updatedAt: Date
}